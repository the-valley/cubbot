package cubBot;

import java.awt.Color;
import java.util.List;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class Commands extends ListenerAdapter{
	public List<TextChannel> detoutliste= MainBot.api.getTextChannelsByName("detout", true);
	public List<Role> everyoneliste= MainBot.api.getRolesByName("@everyone", true);
	public List<TextChannel> annonces= MainBot.api.getTextChannelsByName("annonces-a-la-plebe", true);
	//public String evid =everyoneliste.get(0).getId();
	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		String[] command = event.getMessage().getContent().split(" ");
		String author = event.getAuthor().getName() ;
	
		if(!command[0].startsWith(Ref.PREFIX))
			return;
	
		if(command[0].equalsIgnoreCase(Ref.PREFIX+"ping")) {
			System.out.println("Pong");
			String msg = "Pong! `" + event.getJDA().getPing()+ " ms`";
			if(command.length==1) {
				EmbedBuilder eb = new EmbedBuilder();
				eb.setColor(Color.CYAN);
				eb.setDescription(msg);
				event.getChannel().sendMessage(eb.build()).queue();
			} //Exemple d'option
			else if (command.length == 2 && command[1].equalsIgnoreCase("-e")) {
				EmbedBuilder eb = new EmbedBuilder();
				eb.setColor(Color.CYAN);
				eb.setDescription(msg);
				event.getChannel().sendMessage(eb.build()).queue();
			}
			
		}
		//Commande Game
		if(command[0].equalsIgnoreCase(Ref.PREFIX+"game")) {
			String msgerror = "Erreur: Argument manquant";
		
			if(command.length==1) {
				EmbedBuilder eb = new EmbedBuilder();
				eb.setColor(Color.RED);
				eb.setDescription(msgerror);
				event.getChannel().sendMessage(eb.build()).queue();
			} else if (command.length == 2) {
				List<Role> role = MainBot.api.getRolesByName(command[1], true);
				String id=role.get(0).getId();
				EmbedBuilder eb = new EmbedBuilder();
				eb.setColor(Color.YELLOW);
				eb.setDescription(author+" vous invite � le rejoindre sur "+"<@&"+id+">  !" );
				detoutliste.get(0).sendMessage(eb.build()).queue();
			}
			
		}
	}
	public void onGuildMemberJoin (GuildMemberJoinEvent joinevent) {
		String evid =everyoneliste.get(0).getId();
		EmbedBuilder eb= new EmbedBuilder();
		eb.setColor(Color.BLUE);
		eb.setDescription("<@&"+evid+">" +"Un nouveau louveteau se r�veille ! \n 		Bienvenue � " + joinevent.getUser().getName() + " !" );
		detoutliste.get(0).sendMessage(eb.build()).queue();
	}
	public void onGuildMemberLeave (GuildMemberLeaveEvent leavevent) {
		String evid =everyoneliste.get(0).getId();
		EmbedBuilder eb= new EmbedBuilder();
		eb.setColor(Color.BLACK);
		eb.setDescription("<@&"+evid+"> Un jeune loup part fonder une nouvelle meute! \n		 Au revoir, " + leavevent.getUser().getName() + " !" );
		detoutliste.get(0).sendMessage(eb.build()).queue();
	}
}
