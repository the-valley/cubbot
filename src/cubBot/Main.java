package cubBot;

import java.lang.*;
import java.util.*;
import java.io.*;

public class Main{
	public static void main(String[] args){
		System.out.println("starting bot...");
		System.out.println("version: 2");
		MainBot.start();
		while(true){
			try{
				Thread.sleep(30000);
			}catch(Exception e){}
			if(update()){
				System.out.println("update bot...");
				restart();
			}else{
				System.out.println("already up to date");
			}
		}
	}

	public static void restart(){
		restart(true);
	}

	public static void restart(boolean compile){
		int status=-1;
		MainBot.stop();
		if(compile){
			try{
				Process p=Runtime.getRuntime().exec("javac -encoding windows-1252 -cp JDA-3.3.0_260-withDependencies.jar -d bin src/cubBot/*.java");
				BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
				String line;
				while((line=input.readLine())!=null){
					System.out.println("=>"+line);
				}
				status=p.waitFor();
				if(status!=0){
					System.out.println("can't compile new version");
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		MainBot.start();
	}

	public static boolean update(){
		boolean up=false;
		try{
			Process p = Runtime.getRuntime().exec("git pull origin master");
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while((line=input.readLine())!=null){
				if(line.indexOf("Already up-to-date")==-1){
					up=true;
				}
			}
			int status=p.waitFor();
		}catch(Exception e){
		}
		return up;
	}
}
