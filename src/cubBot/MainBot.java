package cubBot;

import javax.security.auth.login.LoginException;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.hooks.ListenerAdapter;


public class MainBot extends ListenerAdapter
{
	public static JDA api;
	
	public static void start(){
		try {
			api =new JDABuilder(AccountType.BOT).setToken(Ref.TOKEN).buildBlocking();
			api.getPresence().setGame(Game.of("Surveille les louveteaux"));
			api.addEventListener(new Commands());
			api.addEventListener(new MainBot());
			System.out.println(MainBot.api.getRoles());
			
		} catch (LoginException | IllegalArgumentException | InterruptedException | RateLimitedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
   
   	public static void stop(){
		api.shutdown();
	}
}
